import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/future/image'
import { useEffect, useState } from 'react'
import { createClient } from '@supabase/supabase-js'
import moment from 'moment'

const env = process.env.NODE_ENV

const options = {
    schema: 'public',
    autoRefreshToken: true,
    persistSession: true,
    detectSessionInUrl: true
}

const supabase = createClient(process.env.NEXT_PUBLIC_SUPABASE_URL, 
                              process.env.NEXT_PUBLIC_SUPABASE_ANON_KEY, 
                              options)

const { data, error } = await supabase
    .from('release-info')
    .select(`
        *,
        artist-info (*)
    `)
    .match({private: false})
    .order('release_date', { ascending: false })
    .limit(1)

let curRelease = data[0];

const releaseCover = curRelease.cover_url;
const artistPicture = curRelease["artist-info"].image;

function Home() {
    return (
        <>
        <Head>
            <title>Home - ASCEND</title>
        </Head>

        <div className="relative container overflow-hidden h-[300px] bg-neutral-900">
            <div className="relative w-[calc(100%_-_120px)] h-full max-h-[calc(100%_-_60px)] mt-[30px] mb-[30px] mx-[60px] z-20 grid grid-cols-1 grid-rows-1 items-center justify-center">
                <Link href={`${curRelease.stream_link}`}><a target="_blank"><div className="relative w-full py-20 text-center drop-shadow-sm select-none pointer-events-none">
                    <span className="text-xs uppercase font-bold opacity-75 leading-[0px] select-none pointer-events-none">NEW {curRelease.type} OUT NOW</span>
                    <h1 className="block text-5xl font-bold my-4 leading-[36px]">{curRelease.title}</h1>
                    <span className="block leading-[30px]"><Image className="inline mx-2 select-none pointer-events-none rounded-full w-[30px]" src={artistPicture} alt={curRelease["artist-info"].name + " profile picture"} width={60} height={60} quality={25}></Image><b>{curRelease["artist-info"].name}</b>&nbsp;&nbsp;&middot;&nbsp;&nbsp;<span className="opacity-50">Released {moment(curRelease.release_date).format("MMMM Do, YYYY")}</span></span>
                </div></a></Link>
            </div>
            <div className="absolute z-10 w-[50%] h-full top-0 right-0 bg-gradient-to-l from-black"></div>
            <div className="absolute z-10 w-[50%] h-full top-0 left-0 bg-gradient-to-r from-black"></div>
            <div className="absolute z-10 w-full h-full top-0 left-0 bg-gradient-to-t from-black"></div>
            <Image className="absolute w-full top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] z-0 select-none pointer-events-none" src={releaseCover} alt={curRelease.title + " cover art"} width={3000} height={3000} quality={100}></Image>
        </div>

        <h2>EMBRACING THE WEIRD</h2>
        </>
    )
}

export default Home