import Link from 'next/link'

function Header() {
    return (
        <>
        <header className="fixed container z-50 top-0 h-[60px] grid grid-cols-[72px_172.5px_minmax(400px,_1fr)] grid-rows-1 bg-white bg-right bg-cover bg-no-repeat backdrop-blur-sm backdrop-brightness-200">
            <Link href="/"><a title="Homepage"><div className="relative w-[72px] h-[60px] bg-[url('/img/header-logo.webp')] bg-[length:72px] bg-no-repeat"></div></a></Link>
            <Link href="/"><a title="Homepage"><div className="relative w-[171.5px] h-[40px] my-[10px] bg-[url('/img/header-wordmark.webp')] bg-[length:171.5px_60px] bg-left bg-no-repeat"></div></a></Link>
            <nav className="relative text-right mr-[24px]">
                <Link href="/"><a className="header-nav-link">HOME</a></Link>
                <Link href="/about"><a className="header-nav-link">ABOUT US</a></Link>
                <Link href="/roster"><a className="header-nav-link">ARTISTS</a></Link>
                <Link href="/catalog"><a className="header-nav-link">RELEASES</a></Link>
                <Link href="/contact"><a className="header-nav-link">CONTACT US</a></Link>
            </nav>
        </header>
        </>
    )
}

export default Header