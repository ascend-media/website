import { Html, Head, Main, NextScript } from 'next/document'
import Header from "./header";
import Footer from "./footer";

function Document() {
    return (
        <Html className="bg-black text-white">
            <Head>
                <link rel="stylesheet" type="text/css" href="/app.css"/>
                <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
            </Head>

            <body className="md:container mx-auto mt-[60px] bg-black min-h-[calc(100vh_-_60px)] overflow-x-hidden z-10">
                <div className="fixed block z-0 h-[60px] w-screen bg-white top-0 left-0"></div>
                <Header></Header>
                <Main />
                <Footer />
                <NextScript />
            </body>
        </Html>
    )
}

export default Document