const isProd = process.env.NODE_ENV === 'production'

module.exports = {
    webpack: (config) => {
      config.experiments = { ...config.experiments, ...{ topLevelAwait: true }};
      return config;
    },
    experimental: {
        images: {
            allowFutureImage: true
        }
    },
    images: {
        domains: ['ronsggnmsgmuouailams.supabase.co'],
    }
  };